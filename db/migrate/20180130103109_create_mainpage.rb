class CreateMainpage < ActiveRecord::Migration
  def change
    create_table :mainpages do |t|
      t.string :title
      t.text :title_description
      t.text :video_url
      t.text :about_description
      t.text :appointment_title
      t.text :appointment_description
      t.string :appointment_image
      t.text :action_title
      t.text :action_description
      t.string :action_image
      t.text :our_works_description
    end
    Mainpage.create_translation_table! :title => :string, :title_description => :text, :about_description => :text, :appointment_title => :text, :appointment_description => :text, :action_title => :text, :action_description => :text, :our_works_description => :text
  end
  def down
    drop_table :mainpages
    Mainpage.drop_translation_table!
  end
end
