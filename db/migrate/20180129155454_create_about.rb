class CreateAbout < ActiveRecord::Migration
  def up
    create_table :abouts do |t|
      t.text :main_description
      t.integer :projects
      t.integer :years
      t.integer :workers
      t.text :second_description
      t.text :second_frase
      t.text :third_description
      t.text :diller_description
      t.text  :projects_description
    end
    About.create_translation_table! :main_description => :text, :second_description => :text, :second_frase => :text, :third_description => :text, :diller_description => :text, :projects_description => :text
  end
  def down
    drop_table :abouts
    About.drop_translation_table!
  end
end
