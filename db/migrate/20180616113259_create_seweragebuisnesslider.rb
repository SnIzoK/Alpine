class CreateSeweragebuisnesslider < ActiveRecord::Migration
  def change
    create_table :seweragebuisnessliders do |t|
      t.string :title
      t.text :description 
      t.string :image
      t.boolean :show
    end
    Seweragebuisnesslider.create_translation_table! :description => :text, :title => :string
  end
  end
