class CreateSeweragepromslider < ActiveRecord::Migration
  def change
    create_table :seweragepromsliders do |t|
      t.string :title
      t.text :description 
      t.string :image
      t.boolean :show
    end
    Seweragepromslider.create_translation_table! :description => :text, :title => :string
  end
end
