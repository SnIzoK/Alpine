class CreateContactshowroom < ActiveRecord::Migration
  def change
    create_table :contactshowrooms do |t|
      t.string :adress
      t.string :first_number
      t.string :second_number
      t.string :third_number
      t.string :first_email
      t.string :second_email
      t.string :third_email
      t.float :lat
      t.float :lon
    end
    Contactshowroom.create_translation_table! :adress => :string
  end
    def down
    drop_table :contactshowrooms
    Contactshowroom.drop_translation_table!
    end
end
