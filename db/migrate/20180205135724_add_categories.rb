class AddCategories < ActiveRecord::Migration
  def change
    add_column :portfolios, :categories, :string
  end
end
