class CreateShop < ActiveRecord::Migration
  def change
    create_table :shops do |t|
      t.string :title
      t.text :nasos_description
      t.text :vodovidvid_description
      t.text :pool_description
      t.text :warm_description
    end
    Shop.create_translation_table! :title => :string, :nasos_description => :text, :vodovidvid_description => :text, :pool_description => :text, :warm_description => :text
  end
    def down
    drop_table :shops
    Shop.drop_translation_table!
    end
end
