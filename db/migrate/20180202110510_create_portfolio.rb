class CreatePortfolio < ActiveRecord::Migration
  def change
    create_table :portfolios do |t|
      t.string :title
      t.text :description
    end
    Portfolio.create_translation_table! :title => :string, :description => :text
  end
    def down
    drop_table :portfolios
    Portfolio.drop_translation_table!
    end
end
