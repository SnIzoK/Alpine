class CreateSeweragepage < ActiveRecord::Migration
  def change
    create_table :seweragepages do |t|
      t.string :title
      t.text :title_description
      t.string :first_model_title
      t.string :first_model_image
      t.text :first_model_description
      t.string :second_model_title
      t.string :second_model_image
      t.text :second_model_description
      t.string :third_model_title
      t.string :third_model_image
      t.text :third_model_description
      t.string :fourth_model_title
      t.string :fourth_model_image
      t.text :fourth_model_description
      t.string :fifth_model_title
      t.string :fifth_model_image
      t.text :fifth_model_description
      t.string :sixth_model_title
      t.string :sixth_model_image
      t.text :sixth_model_description
      t.string :seventh_model_title
      t.string :seventh_model_image
      t.text :seventh_model_description
      t.string :first_description_image
      t.string :first_description_title
      t.string :second_description_image
      t.string :second_description_title
      t.string :third_description_image
      t.string :third_description_title
      t.string :fourth_description_image
      t.string :fourth_description_title
      t.string :fifth_description_image
      t.string :fifth_description_title
      t.string :sixth_description_image
      t.string :sixth_description_title
      t.text :specific_description
      t.string :first_specific_title
      t.text :first_specific_description
      t.string :second_specific_title
      t.text :second_specific_description
      t.string :third_specific_title
      t.text :third_specific_description
      t.string :fourth_specific_title
      t.text :fourth_specific_description
      t.string :fifth_specific_title
      t.text :fifth_specific_description
      t.text :service_description
      t.string :first_service_title
      t.text :first_service_description
      t.string :second_service_title
      t.text :second_service_description
      t.string :third_service_title
      t.text :third_service_description
    end
       Seweragepage.create_translation_table! :title => :string, :title_description => :text,
         :first_model_title => :string, :first_model_description => :text,
         :second_model_title => :string, :second_model_description => :text,
         :third_model_title => :string, :third_model_description => :text,
         :fourth_model_title => :string, :fourth_model_description => :text,
         :fifth_model_title => :string, :fifth_model_description => :text, 
         :sixth_model_title => :string, :sixth_model_description => :text,
         :seventh_model_title => :string, :seventh_model_description => :text,
         :first_description_title => :string, :second_description_title => :string,
         :third_description_title => :string, :fourth_description_title => :string,
         :fifth_description_title => :string, :sixth_description_title => :string,
         :specific_description => :text,
         :first_specific_title =>  :string, :first_specific_description => :text,
         :second_specific_title =>  :string,  :second_specific_description => :text,
         :third_specific_title =>  :string, :third_specific_description => :text,
         :fourth_specific_title =>  :string, :fourth_specific_description => :text,
         :fifth_specific_title =>  :string, :fifth_specific_description => :text,
         :first_service_title => :string, :first_service_description => :text,
         :second_service_title => :string, :second_service_description => :text, 
         :third_service_title => :string, :third_service_description => :text
  end
end

