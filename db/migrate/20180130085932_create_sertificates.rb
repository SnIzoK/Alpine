class CreateSertificates < ActiveRecord::Migration
  def change
    create_table :sertificates do |t|
      t.text :description
      t.date :date
      t.boolean :show
      t.string :image
    end
    Sertificate.create_translation_table! :description => :text
  end
  def down
    drop_table :sertificates
    Sertificate.drop_translation_table!
  end
end
