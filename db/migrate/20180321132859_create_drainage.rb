class CreateDrainage < ActiveRecord::Migration
  def change
    create_table :drainages do |t|
      t.string :title
      t.text :title_description
      t.text :principes_description
      t.string :first_advantage_title
      t.text :first_advantage
      t.string :second_advantage_title
      t.text :second_advantage
      t.string :third_advantage_title
      t.text :third_advantage
      t.string :fourth_advantage_title
      t.text :fourth_advantage
      t.string :consultation_title
      t.text :consultation_description
    end
    Drainage.create_translation_table! :title => :string, :title_description => :text, :principes_description => :text, :first_advantage_title => :string,
     :second_advantage_title => :string, :third_advantage_title => :string, :fourth_advantage_title => :string, :first_advantage => :text, :second_advantage => :text,
     :third_advantage => :text, :fourth_advantage => :text, :consultation_title => :string, :consultation_description => :text
  end
  def down
    Drainage.drop_translation_table!
    drop_table :drainages
  end
end
