class CreateBlog < ActiveRecord::Migration
  def up
    create_table :blogs do |t|
      t.string :title
      t.text :main_description
      t.date :date
      t.string :main_image
      t.string :second_title
      t.text :second_description
      t.string :third_title
      t.text :third_description
      t.string :frase
      t.string :second_image
      t.string :fourth_title
      t.text :fourth_description
      t.boolean :pool
      t.boolean :clean
      t.boolean :nasos
      t.boolean :warm_nasos
      t.boolean :show
    end
    Blog.create_translation_table! :title => :string, :main_description => :text, :second_title => :string, :second_description => :text, :third_title => :string, :third_description => :text, :frase => :string, :fourth_title => :string, :fourth_description => :text
  end
    def down
      Blog.drop_translation_table!
      drop_table :blogs
    end
end
