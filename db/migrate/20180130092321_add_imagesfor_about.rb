class AddImagesforAbout < ActiveRecord::Migration
  def change
    add_column :abouts, :first_image, :string
    add_column :abouts, :second_image, :string
    add_column :abouts, :buildings_count, :integer
    add_column :abouts, :third_image, :string
  end
end
