class CreateAutonomy < ActiveRecord::Migration
  def change
    create_table :autonomies do |t|
      t.string :title
      t.text :title_description
      t.text :private_description
      t.text :buisness_description
      t.text :prom_description
      t.text :types_description
      t.text :preference_description
      t.string :image
    end
    Autonomy.create_translation_table! :title => :string, :title_description => :text, :private_description => :text, :buisness_description => :text, :prom_description => :text, :types_description => :text, :preference_description => :text
  end
  def down
    drop_table :autonomies
    Autonomy.drop_translation_table!
  end
end
