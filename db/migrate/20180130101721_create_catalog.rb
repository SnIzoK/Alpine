class CreateCatalog < ActiveRecord::Migration
  def change
    create_table :catalogs do |t|
      t.text :first_description
      t.text :second_description
      t.text :third_description
      t.text :fourth_description
    end
    Catalog.create_translation_table! :first_description => :text, :second_description => :text, :third_description => :text, :fourth_description => :text
  end
  def down
    drop_table :catalogs
    Catalog.drop_translation_table!
  end
end
