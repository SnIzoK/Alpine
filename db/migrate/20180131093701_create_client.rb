class CreateClient < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.string :logo
      t.string :title
      t.string :position
      t.text :description
      t.string :image
      t.boolean :show
    end
    Client.create_translation_table! :title => :string, :position => :string, :description => :text
  end
  def down
    drop_table :clients
    Client.drop_translation_table!
  end
end
