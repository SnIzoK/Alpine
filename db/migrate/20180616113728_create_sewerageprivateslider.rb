class CreateSewerageprivateslider < ActiveRecord::Migration
  def change
    create_table :sewerageprivatesliders do |t|
      t.string :title
      t.text :description 
      t.string :image
      t.boolean :show
    end
    Sewerageprivateslider.create_translation_table! :description => :text, :title => :string
  end
end
