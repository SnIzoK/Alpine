class CreateContactmain < ActiveRecord::Migration
  def up
    create_table :contactmains do |t|
      t.string :adress
      t.string :first_number
      t.string :second_number
      t.string :third_number
      t.string :first_email
      t.string :second_email
      t.string :third_email
      t.float :lat
      t.float :lon
    end
    Contactmain.create_translation_table! :adress => :string
  end
    def down
    drop_table :contactmains
    Contactmain.drop_translation_table!
    end
end
