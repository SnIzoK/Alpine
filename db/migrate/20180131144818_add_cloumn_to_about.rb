class AddCloumnToAbout < ActiveRecord::Migration
  def change
    add_column :abouts, :projects_title, :string
    add_column :abouts, :main_title, :string
    reversible do |dir|
     dir.up do
       About.add_translation_fields! main_title: :string
     end
   end
  end
end
