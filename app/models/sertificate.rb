class Sertificate < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  translates :description
  accepts_nested_attributes_for :translations, allow_destroy: true
  scope :show, -> {where(show: true)}
end
