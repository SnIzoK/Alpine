class PortfolioPhoto < ActiveRecord::Base
  mount_uploader :fimage, ImageUploader
  belongs_to :portfolio_photos
end
