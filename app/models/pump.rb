class Pump < ActiveRecord::Base
  translates :title, :title_description, :principes_description, :first_advantage_title, :first_advantage,
  :second_advantage_title, :second_advantage, :third_advantage_title, :third_advantage, :fourth_advantage_title,
  :fourth_advantage, :consultation_title, :consultation_description
  accepts_nested_attributes_for :translations, allow_destroy: true
end
