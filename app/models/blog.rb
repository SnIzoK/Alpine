class Blog < ActiveRecord::Base
  translates :title, :main_description, :second_title, :second_description, :third_title, :third_description, :frase, :fourth_title, :fourth_description
	accepts_nested_attributes_for :translations, allow_destroy: true
  mount_uploader :main_image, ImageUploader
  mount_uploader :second_image, ImageUploader

end
