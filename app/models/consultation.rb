class Consultation < ActiveRecord::Base
	after_create :notify_admin
	validates :phone, presence: true
	validates :name, presence: true
	validates :email, presence: true
	def notify_admin
		UserMailer.consultation_email(self).deliver_now
	end
end
