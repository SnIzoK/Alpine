class Client < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  mount_uploader :logo, ImageUploader
  translates :title, :position, :description
  accepts_nested_attributes_for :translations, allow_destroy: true
  scope :show, -> {where(show: true)}
end
