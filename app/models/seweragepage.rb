class Seweragepage < ActiveRecord::Base
  translates   :title, :title_description, 
  :first_model_title, :first_model_description, 
  :second_model_title,:second_model_description, 
  :third_model_title,   :third_model_description,
   :fourth_model_title,  :fourth_model_description,
    :fifth_model_title, :fifth_model_description, 
    :sixth_model_title, :sixth_model_description,
   :seventh_model_title, :seventh_model_description, 
   :first_description_title, :second_description_title,
    :third_description_title, :fourth_description_title,
   :fifth_description_title, :sixth_description_title, 
   :specific_description,
    :first_specific_title,:first_specific_description, 
    :second_specific_title,:second_specific_description,
   :third_specific_title, :third_specific_description,
   :fourth_specific_title, :fourth_specific_description,
    :fifth_specific_title, :fifth_specific_description,
    :service_description,
    :first_service_title, :first_service_description,
     :second_service_title, :second_service_description, 
     :third_service_title, :third_service_description
  accepts_nested_attributes_for :translations, allow_destroy: true
  
  mount_uploader :first_model_image, ImageUploader
  mount_uploader :second_model_image, ImageUploader
  mount_uploader :third_model_image, ImageUploader
  mount_uploader :fourth_model_image, ImageUploader
  mount_uploader :fifth_model_image, ImageUploader
  mount_uploader :sixth_model_image, ImageUploader
  mount_uploader :seventh_model_image, ImageUploader
  mount_uploader :first_description_image, ImageUploader
  mount_uploader :second_description_image, ImageUploader
  mount_uploader :third_description_image, ImageUploader
  mount_uploader :fourth_description_image, ImageUploader
  mount_uploader :fifth_description_image, ImageUploader
  mount_uploader :sixth_description_image, ImageUploader

end



