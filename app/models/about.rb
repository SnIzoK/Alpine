class About < ActiveRecord::Base
  translates :main_description, :second_description, :second_frase, :third_description, :diller_description, :projects_description, :projects_title, :main_title
	accepts_nested_attributes_for :translations, allow_destroy: true
  mount_uploader :first_image, ImageUploader
  mount_uploader :second_image, ImageUploader
  mount_uploader :third_image, ImageUploader
end
