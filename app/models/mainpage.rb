class Mainpage < ActiveRecord::Base
  mount_uploader :appointment_image, ImageUploader
  mount_uploader :action_image, ImageUploader
  translates :title, :title_description, :about_description, :appointment_description, :appointment_title, :action_title, :action_description, :our_works_description
  accepts_nested_attributes_for :translations, allow_destroy: true
end
