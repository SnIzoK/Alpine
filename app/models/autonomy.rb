class Autonomy < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  translates :title, :title_description, :private_description, :buisness_description, :prom_description, :types_description, :preference_description
  accepts_nested_attributes_for :translations, allow_destroy: true
end
