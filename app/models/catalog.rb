class Catalog < ActiveRecord::Base
  translates :first_description, :second_description, :third_description, :fourth_description
  accepts_nested_attributes_for :translations, allow_destroy: true
end
