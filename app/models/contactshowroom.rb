class Contactshowroom < ActiveRecord::Base
  translates :adress
  accepts_nested_attributes_for :translations, allow_destroy: true
end
