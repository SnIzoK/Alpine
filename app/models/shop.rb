class Shop < ActiveRecord::Base
  translates :title, :nasos_description, :vodovidvid_description, :pool_description, :warm_description
  accepts_nested_attributes_for :translations, allow_destroy: true
  has_many :portfolio_photos
end
