#= require jquery
#= require jquery-ui
#= require jquery_ujs

#= require global

#     P L U G I N S

#= require plugins/jquery-easing
#= require plugins/jquery.appear
#= require plugins/clickout
#= require plugins/datepick
#= require plugins/form
#= require plugins/jquery.bxslider
#= require plugins/jquery.scrolldelta
#= require plugins/lightgallery.min
#= require plugins/lg-thumbnail.min
#= require plugins/jquery.form.min

#= require plugins/TweenMax.min
#= require plugins/TimelineMax.min
#= require plugins/TimelineLite.min

#= require plugins/ScrollMagic.min
#= require plugins/animation.gsap.min
#= require plugins/scrol
#= require plugins/main-animation
#= require plugins/ScrollToPlugin.min

# require plugins/scroll-banner
#= require plugins/selectize.min
#= require plugins/parallax.min
#= require plugins/owlCarousel
#= require plugins/owl.carousel.min
#= require plugins/bx
#= require plugins/jquery.nice-select
#= require plugins/nc
#= require plugins/mixitup-2.1.1.min
#= require plugins/mixItUp
#= require plugins/jquery-modal-video.min
#= require plugins/modal-video.min
#= require plugins/modal_video
#= require plugins/lightgallery-initialize


#     I N I T I A L I Z E

#= require google_map
#= require appear-initialize
#= require bxslider
#= require fullpage_banner_height
#= require header
#= require characters_limit
#= require menu
#= require accordion
#= require selectize-initialize
#= require popups
#= require tabs
#= require navigation
#= require links
#= require success-input