var controller = new ScrollMagic.Controller();

// Single item fade in and slide up...

$(".single-tween-item").each(function() {

  var tween = TweenMax.from(this, .7, {
          y: 40,
          autoAlpha: 0,
          delay: 0,
          opacity: 0,
          ease: Power2.easeOut
      }, .1);

  var scene1 = new ScrollMagic.Scene({
          triggerElement: this,
          offset: -100,
          reverse:true
      })
    .setTween(tween)
    .addTo(controller)
    // .addIndicators()
    ;
});


// Fade in and slide up, cascade through out elements

$(".stagger-tween").each(function() {
  
  var stagger = TweenMax.staggerFrom($(this).find(".stagger-tween-item"), .3, {
    y: 40,
    opacity: 0,
    // ease: Power1.easeOut
    ease: Back.easeIn.config(1.7)
  },
  0.2);

  var scene2 = new ScrollMagic.Scene({
          triggerElement: this,
          offset: -100,
          reverse:true
      })
    .setTween(stagger)
    .addTo(controller)
    // .addIndicators()
    ;
});

// LH Content fade and slide in from right, RH Content fade and slide up...

$(".content-tween").each(function() {
  
  var contentTweenTL = new TimelineMax({
    repeat:0,
  });
  
  var contentTween = contentTweenTL.from($(this).find(".content-tween-left"), .3, {
    x: "-100%",
  }, .1)
  .from($(this).find(".content-tween-right"), .3, {
    x: "100%",
  }, .1)
  .from($(this).find(".title-anim"), .5, {
    y: 40,
    opacity:0
  }, .1)

  .from($(this).find(".text-anim"), 0.7, {
    y: 40,
    opacity:0
  }, .2)

  .from($(this).find(".btn-anim"), 0.3, {
    opacity:0,
    scale:0
  });

  var scene3 = new ScrollMagic.Scene({
          triggerElement: this,
          offset: -100,
          reverse:true
      })
    .setTween(contentTween)
    .addTo(controller)
    // .addIndicators()
    ;
});


$(".tl-anim").each(function() {
  var connectTweenTLM = new TimelineMax({
    repeat:0,
  });

  var tl = new TimelineMax();
    tl.from(".one-anim", 0.3, {y:100, opacity: 0})
    tl.from(".two-anim", 0.3, {y:100, opacity: 0})
    tl.staggerFrom(".four-anim", 0.3, {y:100, opacity:0}, 0.2)
    tl.staggerFrom((".three-anims"), .3, {
    y: 40,
    opacity: 0
    // ease: Power2.easeOut 
  },
  0.2)
    tl.from(".btn-anim", 0.3, {scale:0, opacity:0});

  var scene5 = new ScrollMagic.Scene({
          triggerElement: this,
          offset: -100
      })
    .setTween(tl)
    .addTo(controller)
    ;
});

//// P A R A LA X    3 D 

var SlideParallaxScene4 = new ScrollMagic.Scene({
  triggerElement: ".trigger1",
  triggerHool: 1,
  duration: "100%"
})

  .setTween(TweenMax.from('.parallax-img', 1,{y: "-150%", ease:Power0.easeNone}))
  .addTo(controller);

