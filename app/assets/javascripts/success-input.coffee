$document.on 'submit', 'form.send-form', (e)->
  e.preventDefault();
  $form = $(this)
  $(this).ajaxSubmit({
    type: "POST"
    success: (data) ->
      if $(".popup-advice").hasClass('visible')
        $('.popup-advice').removeClass('visible')
        $('body').removeClass('opened-popup')
      if $(".be-dealer-popup").hasClass('visible')
        $('.be-dealer-popup').removeClass('visible')
        $('body').removeClass('opened-popup')

      $(".success").addClass("visible")
      $('body').addClass('opened-popup')
      setTimeout (->
        $(".success").removeClass("visible")
        $('body').removeClass('opened-popup')
        ), 3000
      $(this)[0].reset();
  })