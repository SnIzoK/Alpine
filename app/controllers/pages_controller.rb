class PagesController < ApplicationController
before_action :initialize_locale_links, except: [:index, :blog_page]
  def index
    @main = Mainpage.first
    @review = Client.all.show
    @portfolios = Portfolio.all
    initialize_locale_links("root")
  end

  def about_us
    @about = About.first
    @main = Mainpage.first
    @review = Client.limit(7)
    @sertificate = Sertificate.all.show
    @projects = Portfolio.all
  end

  def our_projekts
    @portfolios = Portfolio.all
  end

  def blog_page
    @articles = Blog.all
  end

  def article_blog_page
    @articles = Blog.all
    @article = Blog.find(params[:id])
    @locale_links = {}
    I18n.available_locales.each do |locale|
      @locale_links[locale.to_sym] = send("#{action_name}_#{locale}_path", id: @article.id)
    end
  end

  def catalog_page
    @catalog = Shop.first
  end

  def contact_us
    @contactmain= Contactmain.first
    @contactshowroom= Contactshowroom.first
    @contactagency= Contactagency.first
  end

  def sewerage_page
    @autonomy = Autonomy.first
  end
  def pumps
    @one_catalog_value = Pump.first
  end
  def heatpumps
    @one_catalog_value = Heatpump.first
  end
  def pools
    @one_catalog_value = Pool.first
  end
  def drainage
    @one_catalog_value = Drainage.first
  end

  def sewerage_private
    @main = Seweragepage.first
    @slider = Sewerageprivateslider.all.show
  end
  def sewerage_prom
    @main = Sewerageprom.first
    @slider = Seweragepromslider.all.show
  end
  def sewerage_buisnes
    @main = Seweragebuisnes.first
    @slider = Seweragebuisnesslider.all.show
  end

  private

  def set_locale
    redirect_to root_path(locale: I18n.default_locale) if params[:locale].blank?

    I18n.locale = params[:locale] if params[:locale].present?
  end

  def initialize_locale_links(route_name = nil)
    @locale_links = {}
    route_name ||= action_name
    I18n.available_locales.each do |locale|
      @locale_links[locale.to_sym] = send("#{route_name}_#{locale}_path")
    end
  end

  def consultation
  end

  def dealer
  end
end
