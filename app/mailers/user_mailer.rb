class UserMailer < ApplicationMailer
  default from: ENV["smtp_gmail_user_name"]

 def consultation_email(consultation)
   @consultation = consultation
   @url  = 'http://example.com/login'
   mail(to: @consultation.email, subject: 'Заявка на консультацію прийнята')
 end

 def call_email(call)
   @call = call
   @url  = 'http://example.com/login'
   mail(to: @call.email, subject: 'Заявка на консультацію прийнята')
 end
end
