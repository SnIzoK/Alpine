module ApplicationHelper
  def page_url(locale = I18n.locale)
    send("#{action_name}_#{locale}")
  end
end
