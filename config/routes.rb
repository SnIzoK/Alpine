Rails.application.routes.draw do
 post "call", to: "calls#new"
  mount Ckeditor::Engine => '/ckeditor'
  mount Cms::Engine => '/'
  # get "admin(/*admin_path)", to: redirect{|params| "/#{ I18n.default_locale}/admin/#{params[:admin_path]}"}
 localized do

  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root to: "pages#index"

  controller :pages do
    get "about-us", action: "about_us", as: "about_us"
    # get "contact-us", action: "contact_us"

    get "our-projekts", action: "our_projekts", as: "our_projekts"
    get "blog", action: "blog_page", as: "blog_page"
    get "blog/:id", action: "article_blog_page", as: "article_blog_page"
    get "catalog-page", action: "catalog_page", as: "catalog_page"
    get "contact-us", action: "contact_us", as: "contact_us"
    get "sewerage-page", action: "sewerage_page", as: "sewerage_page"
    get "pumps", action: "pumps", as: "pumps"
    get "heatpumps", action: "heatpumps", as: "heatpumps"
    get "pools", action: "pools", as: "pools"
    get "drainage", action: "drainage", as: "drainage"
    get "sewerage_private", action: "sewerage_private", as: "sewerage_private"
    get "sewerage_prom", action: "sewerage_prom", as: "sewerage_prom"
    get "sewerage_buisnes", action: "sewerage_buisnes", as: "sewerage_buisnes"
    post "dealer", action: "dealer#new"

  end
 controller :consultations do
    post "consultation", action: "new"
  end
 end


  match "*url", to: "application#render_not_found", via: [:get, :post, :path, :put, :update, :delete]
end
