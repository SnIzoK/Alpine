RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = trueda


  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
  config.included_models = [Sewerageprivateslider,Sewerageprivateslider::Translation,Seweragebuisnesslider::Translation,Seweragebuisnesslider,Seweragepromslider::Translation,Seweragepromslider,Seweragebuisnes,Seweragebuisnes::Translation,Heatpump,Heatpump::Translation,Pool,Pool::Translation,Drainage,Drainage::Translation,Pump,Pump::Translation,Call,Contactshowroom,Contactagency,Contactmain,Contactagency::Translation,Contactshowroom::Translation,Contactmain::Translation,Portfolio,Portfolio::Translation,Shop,Shop::Translation,Client,Client::Translation,Autonomy,Autonomy::Translation,Mainpage,Mainpage::Translation,Sertificate,Sertificate::Translation,About,About::Translation]
    config.navigation_static_links = {
     locales: "/file_editor/locales"
  }
  
  config.model Sewerageprivateslider do
    navigation_label "Автономні каналізації"
    label "Слайдер приватні"
      field :translations, :globalize_tabs
    include_fields :show, :image
  end
  config.model Sewerageprivateslider::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :description, :title
  end
  
  config.model Seweragebuisnesslider do
    navigation_label "Автономні каналізації"
    label "Слайдер бізнес"
      field :translations, :globalize_tabs
    include_fields :show, :image
  end
  config.model Seweragebuisnesslider::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :description, :title
  end
  
  config.model Seweragepromslider do
    navigation_label "Автономні каналізації"
    label "Слайдер промислові"
      field :translations, :globalize_tabs
    include_fields :show, :image
  end
  config.model Seweragepromslider::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :description, :title
  end
  config.model Sewerageprom do
    navigation_label "Автономні каналізації"
    label "Промислові"
    field :translations, :globalize_tabs , :service_description
    include_fields :first_model_image,
  :first_model_image,
  :second_model_image,
  :third_model_image,
  :fourth_model_image,
  :fifth_model_image,
  :sixth_model_image,
  :seventh_model_image     
  end
  config.model Sewerageprom::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,:title, :title_description, 
    :first_model_title, :first_model_description, 
    :second_model_title,:second_model_description, 
    :third_model_title,   :third_model_description,
     :fourth_model_title,  :fourth_model_description,
      :fifth_model_title, :fifth_model_description, 
      :sixth_model_title, :sixth_model_description,
     :seventh_model_title, :seventh_model_description, 
     :first_description_title, :second_description_title,
      :third_description_title, :fourth_description_title,
     :fifth_description_title, :sixth_description_title, 
     :specific_description, :service_description,
      :first_specific_title,:first_specific_description, 
      :second_specific_title,:second_specific_description,
     :third_specific_title, :third_specific_description,
     :fourth_specific_title, :fourth_specific_description,
      :fifth_specific_title, :fifth_specific_description,
      :first_service_title, :first_service_description,
       :second_service_title, :second_service_description, 
       :third_service_title, :third_service_description
  end
  
  
  config.model Seweragepage do
    navigation_label "Автономні каналізації"
    label "Приватні будинки"
    field :translations, :globalize_tabs , :service_description
    include_fields :first_model_image,
  :first_model_image,
  :second_model_image,
  :third_model_image,
  :fourth_model_image,
  :fifth_model_image,
  :sixth_model_image,
  :seventh_model_image    
  end
  config.model Seweragepage::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,:title, :title_description, 
    :first_model_title, :first_model_description, 
    :second_model_title,:second_model_description, 
    :third_model_title,   :third_model_description,
     :fourth_model_title,  :fourth_model_description,
      :fifth_model_title, :fifth_model_description, 
      :sixth_model_title, :sixth_model_description,
     :seventh_model_title, :seventh_model_description, 
     :first_description_title, :second_description_title,
      :third_description_title, :fourth_description_title,
     :fifth_description_title, :sixth_description_title, 
     :specific_description, :service_description,
      :first_specific_title,:first_specific_description, 
      :second_specific_title,:second_specific_description,
     :third_specific_title, :third_specific_description,
     :fourth_specific_title, :fourth_specific_description,
      :fifth_specific_title, :fifth_specific_description,
      :first_service_title, :first_service_description,
       :second_service_title, :second_service_description, 
       :third_service_title, :third_service_description
  end
  
  config.model Seweragebuisnes do
    navigation_label "Автономні каналізації"
    label "Бізнес"
    field :translations, :globalize_tabs 
      include_fields :first_model_image,
    :first_model_image,
    :second_model_image,
    :third_model_image,
    :fourth_model_image,
    :fifth_model_image,
    :sixth_model_image,
    :seventh_model_image    
  end
  config.model Seweragebuisnes::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,:title, :title_description, 
    :first_model_title, :first_model_description, 
    :second_model_title,:second_model_description, 
    :third_model_title,   :third_model_description,
     :fourth_model_title,  :fourth_model_description,
      :fifth_model_title, :fifth_model_description, 
      :sixth_model_title, :sixth_model_description,
     :seventh_model_title, :seventh_model_description, 
     :first_description_title, :second_description_title,
      :third_description_title, :fourth_description_title,
     :fifth_description_title, :sixth_description_title, 
     :specific_description, :service_description,
      :first_specific_title,:first_specific_description, 
      :second_specific_title,:second_specific_description,
     :third_specific_title, :third_specific_description,
     :fourth_specific_title, :fourth_specific_description,
      :first_service_title, :first_service_description,
       :second_service_title, :second_service_description, 
       :third_service_title, :third_service_description
  end

  config.model Heatpump do
    navigation_label "Товари"
    label "Теплові насоси"
    field :translations, :globalize_tabs
  end
  config.model Heatpump::Translation do
    visible false
    configure :locale, :hidden
    fields :locale, :title, :title_description, :principes_description, :first_advantage_title, :first_advantage,
    :second_advantage_title, :second_advantage, :third_advantage_title, :third_advantage, :fourth_advantage_title,
    :fourth_advantage, :consultation_title, :consultation_description
  end
  config.model Drainage do
    navigation_label "Товари"
    label "Водовідвідні системи"
    field :translations, :globalize_tabs
  end
  config.model Drainage::Translation do
    visible false
    configure :locale, :hidden
    fields :locale, :title, :title_description, :principes_description, :first_advantage_title, :first_advantage,
    :second_advantage_title, :second_advantage, :third_advantage_title, :third_advantage, :fourth_advantage_title,
    :fourth_advantage, :consultation_title, :consultation_description
  end
  config.model Pool do
    navigation_label "Товари"
    label "Басейни"
    field :translations, :globalize_tabs
  end
  config.model Pool::Translation do
    visible false
    configure :locale, :hidden
    fields :locale, :title, :title_description, :principes_description, :first_advantage_title, :first_advantage,
    :second_advantage_title, :second_advantage, :third_advantage_title, :third_advantage, :fourth_advantage_title,
    :fourth_advantage, :consultation_title, :consultation_description
  end
  config.model Pump do
    navigation_label "Товари"
    label "Насосні станції"
    field :translations, :globalize_tabs
  end
  config.model Pump::Translation do
    visible false
    configure :locale, :hidden
    fields :locale, :title, :title_description, :principes_description, :first_advantage_title, :first_advantage,
    :second_advantage_title, :second_advantage, :third_advantage_title, :third_advantage, :fourth_advantage_title,
    :fourth_advantage, :consultation_title, :consultation_description
  end
  config.model About do
    navigation_label "Cтатичні сторінки"
    label "Про компнаію"
    field :translations, :globalize_tabs
    include_fields :projects, :years, :workers, :first_image, :second_image, :third_image, :buildings_count
  end
  config.model About::Translation do
    visible false
    configure :locale, :hidden
    fields :locale, :main_title, :main_description, :second_description, :second_frase, :third_description, :diller_description, :projects_title, :projects_description
  end
  config.model Sertificate do
    navigation_label "Про компнаію "
    label "Сертифікати"
    configure :date do
      strftime_format do
        "%d.%m.%Y"
      end
    end
    field :translations, :globalize_tabs
    include_fields :date, :show, :image
  end
  config.model Sertificate::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :description
  end
  config.model Client do
    navigation_label "Відгуки"
    label "Наші клієнти"
    field :translations, :globalize_tabs
    include_fields :show, :logo, :image
  end
  config.model Client::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :title, :position, :description
  end
  config.model  Mainpage do
    navigation_label "Cтатичні сторінки"
    label "Головна"
    field :translations, :globalize_tabs
    fields :appointment_image, :action_image, :video_url
  end
  config.model Mainpage::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :title, :title_description, :about_description, :appointment_description, :appointment_title, :action_title, :action_description, :our_works_description
  end
  config.model Autonomy do
    navigation_label "Cтатичні сторінки"
    label "Автономні каналізації"
    field :translations, :globalize_tabs
    include_fields :image
  end
  config.model Autonomy::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :title, :title_description, :private_description, :buisness_description, :prom_description, :types_description, :preference_description
  end
  config.model Shop do
    navigation_label "Cтатичні сторінки"
    label "Каталог"
    field :translations, :globalize_tabs
  end
  config.model Shop::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :title, :nasos_description, :vodovidvid_description, :pool_description, :warm_description
  end
  config.model Portfolio do
    navigation_label "Портфоліо"
    label "Проекти"
    field :translations, :globalize_tabs
    field :portfolio_photos
    field :categories, :enum do
      enum ["Приватні","Для Бізнесу","Промислові"]
    end
  end
  config.model Portfolio::Translation do
    visible false
    configure :locale, :hidden
    fields :locale, :title, :description
  end
  config.model PortfolioPhoto do
    field :fimage
  end

  config.model Blog do
    navigation_label "Блог"
    label "Блог"
    field :translations, :globalize_tabs
    configure :date do
      strftime_format do
        "%d.%m.%Y"
      end
    end
    include_fields :main_image, :second_image, :pool, :clean, :nasos, :warm_nasos, :date
  end
  config.model Blog::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :title, :main_description, :second_title, :second_description, :third_title, :third_description, :frase, :fourth_title, :fourth_description
  end

  config.model Contactmain do
    navigation_label "Контакти"
    label "Головне представництво"
    field :translations, :globalize_tabs
    fields :first_number, :second_number, :third_number, :first_email, :second_email, :third_email, :lat, :lon
  end
  config.model Contactmain::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :adress
  end

  config.model Contactshowroom do
    navigation_label "Контакти"
    label "Шоуруми"
    field :translations, :globalize_tabs
    fields :first_number, :second_number, :third_number, :first_email, :second_email, :third_email, :lat, :lon
  end
  config.model Contactshowroom::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :adress
  end
  config.model Contactagency do
    navigation_label "Контакти"
    label "Агенства"
    field :translations, :globalize_tabs
    fields :first_number, :second_number, :third_number, :first_email, :second_email, :third_email, :lat, :lon
  end
  config.model Contactagency::Translation do
    visible false
    configure :locale, :hidden
    fields :locale,  :adress
  end

  config.model Call do
    navigation_label "Контактні форми"
    label "Заявки на звінок"
    fields :phone
  end
  config.model Consultation do
    navigation_label "Контактні форми"
    label "Заявки на консультацію"
    fields :name, :phone, :email
  end
end
